package ru.nikitavov.bua.message;

public record LoginRequest(String login, String password) {

}
