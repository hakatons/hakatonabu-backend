package ru.nikitavov.bua.message;

public record AuthResponse(String accessToken) {

}
