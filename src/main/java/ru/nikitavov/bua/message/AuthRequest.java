package ru.nikitavov.bua.message;

public record AuthRequest(String login, String password) {

}
