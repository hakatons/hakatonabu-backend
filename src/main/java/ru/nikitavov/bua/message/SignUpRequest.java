package ru.nikitavov.bua.message;

public record SignUpRequest(String name, String login, String password, String phone) {
}
