package ru.nikitavov.bua.payload.request;

public record CreateApplicationRequest(int amount, int year, int bankId) {
}
