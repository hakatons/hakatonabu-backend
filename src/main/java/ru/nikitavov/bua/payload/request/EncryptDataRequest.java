package ru.nikitavov.bua.payload.request;

public record EncryptDataRequest(String data, String key) {
}
