package ru.nikitavov.bua.payload.request;

public record SpecializationSearchRequest(int fieldActivityId, int minPrice, int[] subjects) {

}
