package ru.nikitavov.bua.payload.request;

public record UniversitySearchRequest(int cityId, int universityId, int minPrice, int[] subjects) {

}
