package ru.nikitavov.bua.payload.response;

public record UniversityInfoResponse(int yearFoundation, int studentAmount, int teacherAmount

                                     ) {
}
