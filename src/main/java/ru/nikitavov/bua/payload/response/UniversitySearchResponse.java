package ru.nikitavov.bua.payload.response;

import ru.nikitavov.bua.model.FormEducation;

public record UniversitySearchResponse(String title, String description,
                                       String image, boolean isCommerce,
                                       FormEducation form, int year,
                                       int universityId) {
}
