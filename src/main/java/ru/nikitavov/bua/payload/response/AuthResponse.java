package ru.nikitavov.bua.payload.response;

public record AuthResponse(boolean isLogin) {
}
