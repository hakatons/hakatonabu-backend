package ru.nikitavov.bua.payload.response;

import java.math.BigDecimal;

public record BankInfoResponse(String image, String name, int maxSum, BigDecimal percent) {
}
