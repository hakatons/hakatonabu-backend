package ru.nikitavov.bua.payload.response;

public record SpecializationSearchResponse(String title, String description,
                                           int minPointBudget, int minPointCommerce,
                                           int maxPointBudget, int maxPointCommerce,
                                           int minPrice, int count) {
}
