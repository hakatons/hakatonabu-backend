package ru.nikitavov.bua.model;

public enum FormEducation {
    FULL_TIME, DISTANCE, PART_TIME
}
