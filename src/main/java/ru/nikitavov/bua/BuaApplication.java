package ru.nikitavov.bua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.nikitavov.bua.security.AppProperties;

@EnableJpaRepositories(
)
@EnableConfigurationProperties({AppProperties.class})
@SpringBootApplication
public class BuaApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(BuaApplication.class, args);

        context.getBean(InitData.class).initStatuses();
    }

}
