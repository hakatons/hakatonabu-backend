package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.bua.database.model.*;
import ru.nikitavov.bua.database.repository.*;
import ru.nikitavov.bua.security.data.CurrentUser;
import ru.nikitavov.bua.security.data.UserPrincipal;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("users")
public class UserController {

    private final UserRepository userRepository;

    @GetMapping
    public ResponseEntity<List<User>> all() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @GetMapping("me/{id}")
    public ResponseEntity<User> me(@PathVariable int id) {
        return ResponseEntity.ok(userRepository.findById(id).get());
    }

    @GetMapping("/me")
    public ResponseEntity<User> id(@CurrentUser UserPrincipal user) {
        return ResponseEntity.ok(userRepository.findById(user.getId()).get());
    }




}
