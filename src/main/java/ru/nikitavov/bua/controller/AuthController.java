package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nikitavov.bua.database.model.User;
import ru.nikitavov.bua.database.repository.UserRepository;
import ru.nikitavov.bua.message.AuthResponse;
import ru.nikitavov.bua.message.LoginRequest;
import ru.nikitavov.bua.message.SignUpRequest;
import ru.nikitavov.bua.security.data.UserPrincipal;
import ru.nikitavov.bua.security.service.TokenProvider;
import ru.nikitavov.bua.security.service.UserPrincipalCreator;

import java.util.Optional;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenProvider tokenProvider;
    private final UserPrincipalCreator principalCreator;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {

        Optional<User> optionalUser = userRepository.findByLogin(loginRequest.login());
        if (optionalUser.isEmpty()) return ResponseEntity.notFound().build();

        if (!passwordEncoder.matches(loginRequest.password(), optionalUser.get().getPassword())) {
            return ResponseEntity.badRequest().build();
        }

        UserPrincipal principal = principalCreator.create(optionalUser.get());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(principal,
                null,
                principal.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByLogin(signUpRequest.login())) {
            throw new RuntimeException("Email address already in use.");
        }

        User user = new User();
        user.setFullName(signUpRequest.name());
        user.setLogin(signUpRequest.login());
        user.setPhone(signUpRequest.phone());
        user.setPassword(signUpRequest.password());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User result = userRepository.save(user);

        UserPrincipal principal = principalCreator.create(result);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(principal,
                null,
                principal.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }

}
