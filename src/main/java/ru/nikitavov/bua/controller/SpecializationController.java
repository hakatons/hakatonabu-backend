package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nikitavov.bua.database.repository.SpecializationRepository;
import ru.nikitavov.bua.payload.request.SpecializationSearchRequest;
import ru.nikitavov.bua.payload.request.UniversitySearchRequest;
import ru.nikitavov.bua.payload.response.SpecializationSearchResponse;
import ru.nikitavov.bua.payload.response.UniversitySearchResponse;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("specialties")
public class SpecializationController {

    private final SpecializationRepository specializationRepository;

    @GetMapping("search/field_activity")
    public ResponseEntity<List<SpecializationSearchResponse>> getFieldActivityByFilter(SpecializationSearchRequest request) {
        return ResponseEntity.ok().build();
    }

    @GetMapping("search/university")
    public ResponseEntity<List<UniversitySearchResponse>> getUniversityByFilter(UniversitySearchRequest request) {
        return ResponseEntity.ok().build();
    }


}
