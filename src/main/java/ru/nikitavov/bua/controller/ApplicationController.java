package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.bua.database.model.Application;
import ru.nikitavov.bua.database.repository.ApplicationRepository;
import ru.nikitavov.bua.database.repository.StatusRepository;

import java.util.List;

@RestController
@RequestMapping("/application")
@RequiredArgsConstructor
public class ApplicationController {

    private final ApplicationRepository applicationRepository;
    private final StatusRepository statusRepository;

    @GetMapping
    public ResponseEntity<List<Application>> getApplications() {
        return ResponseEntity.ok(applicationRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Application> getApplication(@PathVariable int id) {
        return ResponseEntity.ok(applicationRepository.findById(id).get());
    }

    @PostMapping("{id}/completed")
    public ResponseEntity<Void> completed(@PathVariable int id) {
        Application application = applicationRepository.findById(id).get();

         application.setStatus(statusRepository.findByName("completed").get());
        applicationRepository.save(application);

        return ResponseEntity.ok().build();
    }
}
