package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.bua.payload.response.UniversityInfoResponse;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("universities")
public class UniversityController {

    @GetMapping("info/{id}")
    public ResponseEntity<UniversityInfoResponse> getUniversityInfoById(@PathVariable int id) {
        return ResponseEntity.ok().build();
    }
}
