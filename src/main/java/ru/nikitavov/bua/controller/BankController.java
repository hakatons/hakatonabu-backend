package ru.nikitavov.bua.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.bua.database.model.Application;
import ru.nikitavov.bua.database.model.Bank;
import ru.nikitavov.bua.database.repository.ApplicationRepository;
import ru.nikitavov.bua.database.repository.BankRepository;
import ru.nikitavov.bua.database.repository.StatusRepository;
import ru.nikitavov.bua.payload.request.CreateApplicationRequest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/bank")
@RequiredArgsConstructor
public class BankController {

    private final BankRepository bankRepository;
    private final StatusRepository statusRepository;
    private final ApplicationRepository applicationRepository;

    @GetMapping
    public ResponseEntity<List<Bank>> getBanks() {
        return ResponseEntity.ok(bankRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Bank> getBank(@PathVariable int id) {
        return ResponseEntity.ok(bankRepository.findById(id).get());
    }

    @PostMapping("/create")
    public ResponseEntity<Void> getInfo(@RequestBody CreateApplicationRequest request) {
        Bank bank = bankRepository.findById(request.bankId()).get();
        Application application = Application.builder()
                .amount(request.amount())
                .year(request.year())
                .bank(bank)
                .dateTime(LocalDateTime.now())
                .status(statusRepository.findByName("approved").get())
                .percent(bank.getPercent().add(BigDecimal.valueOf(new Random().nextInt(50) * 0.1)))
                .build();

        applicationRepository.save(application);
        return ResponseEntity.ok().build();
    }

}
