package ru.nikitavov.bua.response;

public class ResponseDataEmpty extends ResponseDataAbstract<Object> {
    public ResponseDataEmpty() {
        super(null);
    }
}
