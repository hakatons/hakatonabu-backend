package ru.nikitavov.bua;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nikitavov.bua.database.model.Status;
import ru.nikitavov.bua.database.repository.StatusRepository;

@Service
@RequiredArgsConstructor
public class InitData {

    private final StatusRepository statusRepository;

    public void initStatuses() {
        if (statusRepository.findByName("approved").isEmpty()) {
            statusRepository.save(Status.builder().name("approved").build());
        }
        if (statusRepository.findByName("completed").isEmpty()) {
            statusRepository.save(Status.builder().name("completed").build());
        }
    }
}
