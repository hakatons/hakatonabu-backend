package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Bank;

public interface BankRepository extends JpaRepository<Bank, Integer> {
}