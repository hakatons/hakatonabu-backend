package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
}