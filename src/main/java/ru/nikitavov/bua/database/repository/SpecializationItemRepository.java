package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.SpecializationItem;

public interface SpecializationItemRepository extends JpaRepository<SpecializationItem, Integer> {
}