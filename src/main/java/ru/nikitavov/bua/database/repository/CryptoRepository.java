package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Crypto;

import java.util.Optional;

public interface CryptoRepository extends JpaRepository<Crypto, Integer> {
    Optional<Crypto> findByKey(String key);
}