package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}