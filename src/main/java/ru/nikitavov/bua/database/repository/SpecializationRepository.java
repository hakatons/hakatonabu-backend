package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Specialization;

public interface SpecializationRepository extends JpaRepository<Specialization, Integer> {
}