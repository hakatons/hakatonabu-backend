package ru.nikitavov.bua.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.bua.database.model.Application;

public interface ApplicationRepository extends JpaRepository<Application, Integer> {
}