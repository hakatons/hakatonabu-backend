package ru.nikitavov.bua.database.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "application")
public class Application implements IEntity<Integer> {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "bank_id", nullable = false)
    Bank bank;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "status_id", nullable = false)
    Status status;

    @Column(name = "amount", nullable = false)
    int amount;

    @Column(name = "year", nullable = false)
    int year;

    @Column(name = "percent", nullable = false)
    BigDecimal percent;

    @Column(name = "date_time", nullable = false)
    LocalDateTime dateTime;
}
