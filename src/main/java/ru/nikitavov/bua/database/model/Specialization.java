package ru.nikitavov.bua.database.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;


@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "specialization")
public class Specialization implements IEntity<Integer> {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @NotBlank
    @Column(name = "title", nullable = false)
    String title;

    @NotBlank
    @Column(name = "description", nullable = false)
    String description;

    @Column(name = "minPointBudget", nullable = false)
    int minPointBudget;

    @Column(name = "minPointCommerce", nullable = false)
    int minPointCommerce;

    @Column(name = "maxPointBudget", nullable = false)
    int maxPointBudget;

    @Column(name = "maxPointCommerce", nullable = false)
    int maxPointCommerce;

    @Column(name = "minPrice", nullable = false)
    int minPrice;

    @Column(name = "count", nullable = false)
    int count;
}
