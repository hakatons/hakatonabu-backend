package ru.nikitavov.bua.database.model;

public interface IEntityWithName<I> extends IEntity<I>{
    void setName(String name);
}
