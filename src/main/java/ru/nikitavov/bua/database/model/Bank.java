package ru.nikitavov.bua.database.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;


@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bank")
public class Bank implements IEntity<Integer> {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @NotBlank
    @Column(name = "image", nullable = false)
    String image;

    @Column(name = "description", nullable = false)
    String description;

    @Column(name = "maxSum", nullable = false)
    int maxSum;

    @Column(name = "percent", nullable = false)
    BigDecimal percent;
    }
